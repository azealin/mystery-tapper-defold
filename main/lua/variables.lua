-- Put functions in this file to use them in several other scripts.
-- To get access to the functions, you need to put:
-- require "my_directory.my_file"
-- in any script using the functions.
local variables = {}
variables.score = 0
variables.hp = 100
variables.strength = 1
variables.cost = 10

local poofCounter = 1
local poofs = { 
	{ 
		name = 'poof_base_animation', 
		strength = 1,
		hp = 100,
		cost = 10
	}, 
	{
		name = 'poro_snack', 
		strength = 2,
		hp = 110,
		cost = 100
	}
}


function variables.add_score(amount) 
	variables.score = variables.score + amount
	if variables.score > variables.cost and poofs[poofCounter+1] ~= nil then
		evolve_poof()
	end
end

function evolve_poof() 
	poofCounter = poofCounter + 1
	local currentPoof = poofs[poofCounter]
	variables.strength = currentPoof.strength
	variables.hp = currentPoof.hp
	variables.cost = currentPoof.cost

	msg.post("main#score", "set_hp")
	sprite.play_flipbook("poro_1#sprite", currentPoof.name)
end

return variables